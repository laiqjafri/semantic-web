You start with:
  <xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsd:element name="bank" type="bank_type"> // Every xsd:element has a name and a type
    </xsd:element>

    <xsd:complexType name="bank_type">
      <xsd:sequence>
        <xsd:element name="accounts" type="accounts_type" />
        <xsd:element name="customers" type="customers_type" />
        <xsd:element name="customer_accounts" type="customer_accounts_type" />
      </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="accounts_type">
      <xsd:sequence>
        <xsd:element name="savings_accounts" type="savings_accounts_type"/>
        <xsd:element name="checking_accounts" type="checking_accounts_type"/>
      </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="customers_type">
      <xsd:sequence>
        <!-- number of customers can be 0 to unlimited -->
        <xsd:element name="customer" type="customer_type" minOccurs="0" maxOccurs="unbounded"/>
      </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="customer_accounts_type">
      <xsd:sequence>
        <xsd:element name="customer_account" type="customer_account_type" minOccurs="0" maxOccurs="unbounded"/>
      </xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="savings_accounts_type">
      <xsd:sequence>
        <xsd:element name="savings_account" type="savings_account_type" minOccurs="0" maxOccurs="unbounded"/>
      </xsd:sequence>
    </xsd:complexType>
  
    <xsd:complexType name="checking_accounts_type">
      <xsd:sequence>
        <xsd:element name="checking_account" type="checking_account_type" minOccurs="0" maxOccurs="unbounded"/>
      </xsd:sequence>
    </xsd:complexType>

    <!-- specification of the base account type -->
    <xsd:complexType name="account_type">
      <xsd:sequence>
        <xsd:element name="balance" type="balance_type"/>
      </xsd:sequence>
      
      <!-- ids are always "required" -->
      <xsd:attribute name="id" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:simpleType name="balance_type">
        
      <!-- balance can be decimal -->
      <xsd:restriction base="xsd:decimal">
      
        <!--  the balance is greater than -5000, therefore minExclusive -->
        <xsd:minExclusive value="-5000"/>
      </xsd:restriction>
    </xsd:simpleType >   
    
    <xsd:complexType name="savings_account_type">
     <xsd:complexContent>
     
       <!-- extend the base account -->
       <xsd:extension base="basic_account_type">
       
         <!-- interest can be decimal -->
         <xsd:attribute name="interest" type="xsd:decimal" use="required"/>
       </xsd:extension>
      </xsd:complexContent>
    </xsd:complexType>

    <xsd:complexType name="checking_account_type">
     <xsd:complexContent>
       <xsd:extension base="basic_account_type">
       </xsd:extension>
      </xsd:complexContent>
    </xsd:complexType>


    <xsd:complexType name="customer_type">
      <xsd:sequence>
        <xsd:element name="name" type="xsd:string"/>
        <xsd:element name="address" type="xsd:string"/>
      </xsd:sequence>
      <xsd:attribute name="id" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="customer_account_type">
      <xsd:attribute name="c_id" type="xsd:string" use="required"/>
      <xsd:attribute name="ac_id" type="xsd:string" use="required"/>
    </xsd:complexType>
      
  </xsd:schema>

